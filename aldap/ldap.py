import time

from django.core.exceptions import BadRequest

import ldap
from ldap.ldapobject import SimpleLDAPObject

from . import settings
from .logger import logger


class LDAPServer(SimpleLDAPObject):
    SERVER_URI = settings.AUTH_LDAP_SERVER_URI
    CONNECTION_OPTIONS = settings.AUTH_LDAP_SERVER_CONNECTION_SETTING

    START_TLS = settings.AUTH_LDAP_START_TLS

    _connection = None

    @classmethod
    def get_connection(cls, renew=False):
        if cls._connection is not None and not renew:
            return cls._connection

        uri = cls.SERVER_URI
        if callable(uri):
            uri = uri(cls._request)

        cls._connection = ldap.initialize(uri, bytes_mode=False)

        if cls.CONNECTION_OPTIONS:
            for opt, value in cls.CONNECTION_OPTIONS.items():
                cls._connection.set_option(opt, value)
        else:
            cls._connection.set_option(ldap.OPT_REFERRALS, ldap.OPT_OFF)

        if cls.START_TLS:
            logger.debug("Initiating TLS")
            cls._connection.start_tls_s()

        return cls._connection


class LDAPUser:
    def __init__(self, bindDN):
        self.bindDN = bindDN

    def authenticate(self, password):
        self._authenticate_user_dn(password)
        self._check_requirements()

    def _authenticate_user_dn(self, password):
        try:
            server = ldap.initialize(settings.AUTH_LDAP_SERVER_URI, bytes_mode=False)
            logger.info("------------- BIND -------------- 2 %r", self.bindDN)
            result = server.simple_bind_s(self.bindDN, password)
            logger.info("result %r", result)
            server.unbind()
            logger.info("------------- UNBIND -------------- 2")
        except ldap.INVALID_CREDENTIALS:
            raise BadRequest("user DN/password rejected by LDAP server.")

    def _check_requirements(self):
        """
        Checks all authentication requirements beyond credentials. Raises
        AuthenticationFailed on failure.
        """
        self._check_required_group()
        self._check_denied_group()

    def _check_required_group(self):
        """
        Returns True if the group requirement (AUTH_LDAP_REQUIRE_GROUP) is
        met. Always returns True if AUTH_LDAP_REQUIRE_GROUP is None.
        """
        required_group_dn = settings.AUTH_LDAP_USER_REQUIRE_GROUP

        # if required_group_dn is not None:
        #     if not isinstance(required_group_dn, LDAPGroupQuery):
        #         required_group_dn = LDAPGroupQuery(required_group_dn)
        #     result = required_group_dn.resolve(self)
        #     if not result:
        #         raise self.AuthenticationFailed(
        #             "user does not satisfy AUTH_LDAP_REQUIRE_GROUP"
        #         )

        return True

    def _check_denied_group(self):
        """
        Returns True if the negative group requirement (AUTH_LDAP_DENY_GROUP)
        is met. Always returns True if AUTH_LDAP_DENY_GROUP is None.
        """
        denied_group_dn = settings.AUTH_LDAP_USER_DENY_GROUP

        # if denied_group_dn is not None:
        #     is_member = self._get_groups().is_member_of(denied_group_dn)
        #     if is_member:
        #         raise self.AuthenticationFailed(
        #             "user does not satisfy AUTH_LDAP_DENY_GROUP"
        #         )

        return True


class LDAPAuthentication:
    server = LDAPServer(settings.AUTH_LDAP_SERVER_URI)
    ldap_server = server.get_connection()
    logger.info("------------- BIND -------------- 1")
    ldap_server.simple_bind(
        settings.AUTH_LDAP_SERVER_BIND_DN, settings.AUTH_LDAP_SERVER_PASSWORD
    )
    logger.info("result %r", ldap_server.result4())

    def authenticate(self, username, password) -> LDAPUser:
        result = self.ldap_server.search(
            settings.AUTH_LDAP_USER_DN,
            settings.AUTH_LDAP_USER_SCOPE,
            filterstr=f"(&(objectClass=user)(|(mailNickname={username})(mail={username})))",
        )
        res_type, res_data, res_msgid, res_controls, _, _ = self.ldap_server.result4(
            result, 0, -1, add_ctrls=0
        )
        for dn, entry in res_data:
            if dn:
                user = LDAPUser(dn)
                user.authenticate(password)
                return user
        raise BadRequest("Username not true!")
