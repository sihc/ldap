import logging
from datetime import datetime, timedelta

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from django_auth_ldap.backend import LDAPBackend

import ldap
from ldap.dn import str2dn
from ldap.ldapobject import LDAPObject
from ldap.resiter import ResultProcessor

from .ldap import LDAPAuthentication


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


class Log:
    def __init__(self, msg, tag="INFO", type="message"):
        self.tag = tag.lower()
        self.msg = str(msg).replace("\n", "<br>")
        self.type = type


class MyLDAPObject(LDAPObject, ResultProcessor):
    pass


class ALDAP:
    logs = [
        Log("Hello"),
    ]

    @classmethod
    def _context(cls, request):
        if request.method == "GET":
            return {
                "messages": reversed(cls.logs),
                "bindDN": settings.AUTH_LDAP_BIND_DN,
                "scope": ldap.SCOPE_SUBTREE,
                "filter": "(&(objectCategory=person)(objectClass=user))",
            }
        else:
            data = cls._data(request)
            return {
                "messages": reversed(cls.logs),
                **data,
            }

    @staticmethod
    def _data(request):
        return dict(
            bindDN=request.POST["bindDN"],
            scope=int(request.POST["scope"]),
            filter=request.POST["filter"],
        )

    @classmethod
    def command(cls, request):
        log = loader.get_template("aldap/log.html")
        if request.method == "GET":
            return HttpResponse(log.render(cls._context(request), request))
        else:
            cls.logs.append(Log(datetime.utcnow() + timedelta(hours=7), type="time"))
            username = request.POST.get("username", None)
            if username:
                cls.logs.append(Log(username, type="command"))
                try:
                    password = request.POST.get("password", "")
                    result = LDAPAuthentication().authenticate(username, password)
                    cls.logs.append(Log(result))
                except Exception as e:
                    result = str(e)
                    cls.logs.append(Log(result, tag="ERROR"))
                    logging.exception(e)
            else:
                l = MyLDAPObject(settings.AUTH_LDAP_SERVER_URI)
                l.simple_bind_s(
                    settings.AUTH_LDAP_BIND_DN, settings.AUTH_LDAP_BIND_PASSWORD
                )
                try:
                    data = cls._data(request)
                    msg_id = l.search(data["bindDN"], data["scope"], data["filter"])
                    cls.logs.append(Log(str(data), type="command"))
                    result = list(l.allresults(msg_id))
                    cls.logs.append(Log(result))
                except Exception as e:
                    result = str(e)
                    cls.logs.append(Log(result, tag="ERROR"))
                    logging.exception(e)
                l.unbind_s()

            return HttpResponse(log.render(cls._context(request), request))
