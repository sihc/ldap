import ldap

ldap.set_option(ldap.OPT_REFERRALS, ldap.OPT_OFF)
ldap.set_option(ldap.OPT_DEBUG_LEVEL, 4095)

con = ldap.initialize("ldap://10.3.12.57", trace_level=2)
con.simple_bind("CN=KHCP,OU=DK-SC,DC=mobifone,DC=vn", "bds@1234")

username = "mobifone_nbhkd02"
result = con.search(
    "DC=mobifone,DC=vn",
    ldap.SCOPE_SUBTREE,
    f"(&(objectClass=user)(|(mailNickname={username})(mail={username})))",
)
result
a, b, c, d, e, f = con.result4(result, 0, -1, add_ctrls=0)
con.unbind()

con2 = ldap.initialize("ldap://10.3.12.57", trace_level=2)
con2.simple_bind("CN=mobifone_nbhkd02,OU=CN NinhBinh,OU=CTY4,DC=mobifone,DC=vn", "")

# con.result()
# for res_type, res_data, res_msgid, res_controls in MyLDAPObject(
#     "ldap://10.3.12.57"
# ).allresults(result):
#     for dn, entry in res_data:
#         print("df", dn)
