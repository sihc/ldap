[< ..](./README.md)

# INSTALLATION

## 1. Install required libraries for system

### libldap2-dev

Debian/Ubuntu:

> sudo apt-get install libsasl2-dev python-dev libldap2-dev libssl-dev

RedHat/CentOS:

> sudo yum install python-devel openldap-devel

## 2. Install requirement libraries:

> pip install -r requirement.txt
